package coi.cardsofilion;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;

import java.io.IOException;

public class App extends Application {


    //if the song is the start method, it only plays a couple of seconds
    Media music = new Media("file:///C://Users//admin//Desktop//COIFinal//src//main//resources//audio//song.mp3");


    @Override
    public void start(Stage stage) throws IOException {

        //MediaPlayer

        MediaPlayer mediaPlayer = new MediaPlayer(music);
        mediaPlayer.setAutoPlay(true);
        mediaPlayer.setCycleCount(MediaPlayer.INDEFINITE);

        //Loader to load the scenes

        Parent root = FXMLLoader.load(getClass().getResource("scene1.fxml"));
        Scene scene = new Scene(root);

        String css = this.getClass().getResource("/css/scene1.css").toExternalForm();
        scene.getStylesheets().add(css);
        stage.setTitle("CARDS OF ILION");
        Image icon = new Image("/cards/testcard.png");
        stage.getIcons().add(icon);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
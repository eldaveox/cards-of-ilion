package coi.cardsofilion;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.IOException;



public class Controller extends Scene2Controller  {



    @FXML
    TextField nameTextField;
    Button button1;
    ImageView realCard;


    /** This part controls the switches to different stages**/
    @FXML
    private Stage stage;
    private Scene scene;
    private Parent root;
    @FXML
    public void cardsOnOff(ActionEvent event){
        realCard.getImage();
        if (realCard.isVisible()){
            realCard.setDisable(true);;
        }else{
            button1.isPressed();
        }
    }

    public void switchToScene1 (ActionEvent event) throws IOException {
        root = FXMLLoader.load(getClass().getResource("scene1.fxml"));
        stage =(Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public void switchToScene2(ActionEvent event) throws IOException {
        root = FXMLLoader.load(getClass().getResource("scene2.fxml"));
        stage =(Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();;
    }

    public void switchToScene3(ActionEvent event) throws IOException {
        String playerName = nameTextField.getText();


        if (playerName.isEmpty()){
            playerName = "NAMELESS";
        }

        FXMLLoader loader =new FXMLLoader(getClass().getResource("scene3.fxml"));
        root = loader.load();

        Scene2Controller scene2Controller = loader.getController();
        scene2Controller.displayName(playerName);

        stage =(Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /*---------------------------------------------------------------------------*/


}